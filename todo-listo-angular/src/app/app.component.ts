import { Component, OnInit } from '@angular/core';
import { Tarea, EstadoTarea } from './tarea';
import { TareaService } from './tarea.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title = 'Todo Listo!';
  estadoTareas = EstadoTarea;
  tareaSeleccionada: Tarea;
  tareas: Array<Tarea>;
  tareasTotal: Array<Tarea>;
  newTarea: Tarea;
  otherTarea: Tarea;

  rForm: FormGroup;
  post:any;
  id:number = 0;                     // A property for our submitted form
  descripcion:string = '';
  titulo:string = '';

  constructor(public tareaService: TareaService,private fb: FormBuilder) {
    this.tareas = [];
    this.newTarea = new Tarea(null, null, null);
    this.otherTarea = new Tarea(null, null, null);
    this.rForm = fb.group({
      'id': [null],
      'titulo': [null],
      'descripcion': [null]
    });
  }

  ngOnInit() {
    this.tareaService.getTareas()
        .subscribe((ts: Array<Tarea>) => {
          this.tareasTotal = ts;
          this.tareas = ts;
        });
    
  }

  actualizarTarea(t: Tarea) {
    console.log(`La tarea ${t} fue actualizada!`);
    this.tareaSeleccionada = t;
    /*console.log('la tarea seleccionada es: ', this.tareaSeleccionada);*/
    this.tareaService.cambiarEstado(this.tareaSeleccionada).subscribe(_ => {
      this.tareaService.getTareas()
        .subscribe((ts: Array<Tarea>) => {
          this.tareasTotal = ts;
          this.tareas = ts;
        });

    });
  }

  seleccionarTarea(t: Tarea) {
    this.tareaSeleccionada = t;
  }

  crearTarea() {
    console.log(this.newTarea);
    this.tareaService.crearTarea(this.newTarea).subscribe(_ => {

      //this.tareas.push(this.newTarea);
      this.tareaService.getTareas()
        .subscribe((ts: Array<Tarea>) => {
          this.tareasTotal = ts;
          this.tareas = ts;
        });

    });
  }

  modificarTarea(post) {
    this.id= post["id"];
    this.titulo =post["titulo"];
    this.descripcion = post["descripcion"];
    if(this.id==null){
      this.id = this.tareaSeleccionada["id"];
    }
    if(this.titulo == null){
      this.titulo = this.tareaSeleccionada["titulo"];
    }
    if(this.descripcion==null){
      this.descripcion= this.tareaSeleccionada["descripcion"];
    }
    this.tareaSeleccionada.id = this.id;
    this.tareaSeleccionada.descripcion = this.descripcion;
    this.tareaSeleccionada.titulo = this.titulo;

    if(this.tareaSeleccionada.titulo.length < 5) {
      alert("El titulo debe tener largo mayor a 5 caracteres");
    }else if(this.tareaSeleccionada.descripcion.length <10 || this.tareaSeleccionada.descripcion.length>100){
      alert("La descripcion debe tener entre 10 y 100 caracteres");
    }else {
      this.tareaService.modificarTarea(this.tareaSeleccionada).subscribe(_ => {

        //this.tareas.push(this.newTarea);
        this.tareaService.getTareas()
          .subscribe((ts: Array<Tarea>) => {
            this.tareasTotal = ts;
            this.tareas = ts;
          });
      });
    }
    location.reload();
  }

  estado2str(e: EstadoTarea) {
    switch (e) {
      case EstadoTarea.Creada:    return 'Creada';
      case EstadoTarea.EnProceso: return 'En Proceso';
      case EstadoTarea.Terminada: return 'Terminada';
    }
  }

  buscarTarea(s:string){
    let t = [];
    if(s == ""){
      t = this.tareasTotal;
    }else{
      this.tareasTotal.forEach(function(tarea){
        if(tarea["titulo"].includes(s)){
          t.push(tarea);
        }
      });
    }
    this.tareas = t;
  }

}































/*
export class AppComponent implements OnInit {
  title = 'Todo Listo!';
  estadoTareas = EstadoTarea;
  tareaSeleccionada: Tarea;
  tareas: Array<Tarea>;
  newTarea: ITarea;
  estado2str = estado2str;

  constructor(private tareaService: TareaService) {
    this.tareas = [];
    this.newTarea = {
      titulo: '',
      descripcion: ''
    };
  }

  ngOnInit(): void {
    // Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    // Add 'implements OnInit' to the class.
    this.tareaService.getTareas()
        .subscribe(tareas => {
          this.tareas = tareas;
        });
  }

  seleccionarTarea(t: Tarea) {
    this.tareaSeleccionada = t;
  }

  crearTarea() {
    console.log(this.newTarea);
    // TODO: Add loading controller
    this.tareaService.crearTarea(this.newTarea);
  }

  guardarTarea(t: Tarea) {
    console.log(`Guardando tarea: ${t}`);
  }
}
*/