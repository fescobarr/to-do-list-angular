import { Injectable } from '@angular/core';
import { Tarea } from './tarea';
import { Observable, of, empty } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class TareaService {

  constructor(private http: HttpClient) { }

crearTarea(t: Tarea): Observable<Tarea> {
   return this.http.post<Tarea>(
    'http://127.0.0.1:8000/tareas/',
     t,
     httpOptions
    );
  }

  getTareas(): Observable<any> {
    return this.http.get('http://127.0.0.1:8000/tareas/');
  }

  cambiarEstado(t: Tarea): Observable<any>{
    let id= t.id;
    console.log('el id es: '+ id);
    let url= 'http://127.0.0.1:8000/tareas/'+id+'/';
    return this.http.put(
      url,
      t,
      httpOptions
    );
  }

  modificarTarea(t:Tarea): Observable<any>{
    let id = t.id;
    let url= 'http://127.0.0.1:8000/tareas/'+id+'/';
    return this.http.put(
      url,
      t,
      httpOptions
    );
  }

}
