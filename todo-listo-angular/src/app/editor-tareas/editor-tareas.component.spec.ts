import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorTareasComponent } from './editor-tareas.component';

describe('EditorTareasComponent', () => {
  let component: EditorTareasComponent;
  let fixture: ComponentFixture<EditorTareasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorTareasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorTareasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
